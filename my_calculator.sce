//SciLab 6.0.2
//author: Beniamin Strączek
//Laboratory exercises 1
//task 7

//It would be nice and clear when I don't see all the step-by-step calcs on screen

h = 6.62607004e-34; //Planck's constant; 'e-34' is a scientific notation and it stands for 'times 10 to -34th power'
R = 6371; //Earth's radius [km]
N = 6.0221409e+23; //Avogadro's number


//7.1
disp('7.1')
disp('h / (2 * %pi) =') //%pi is SciLab's built in value for pi
disp(h / (2 * %pi))

//7.2
disp('7.2')
disp('sind(30 / %e) =') //%e is SciLab's built in value for Euler's number
disp(sind(30 / %e)) //the argument here is in degrees

//7.3
disp('7.3')
disp('hex2dec(''00123d3'') / 2.455e23 =') //function 'hex2dec' converts hexadecimal number into decimal number
disp(hex2dec('00123d3') / 2.455e23)

//4
disp('7.4')
disp('sqrt(%e - %pi) =') // 'sqrt' function returns the square root of an argument
disp(sqrt(%e - %pi))

//7.5
disp('7.5')
disp('part(msprintf(''%.10f'', %pi), 12) =') //function 'msprintf' converts pi to a string with 10 decimals. Then 'part' function displays 12th character of that string (which is the 10th decimal of pi)
disp(part(msprintf('%.10f', %pi), 12))

//7.6
disp('7.6')
disp('20*365 + 5 + (14-4) =') //days of 20 yrs + 5 days of leap yrs + (actual day - birth day); last update: 2019-10-14
disp(20*365 + 5 + (14-4))

//7.7
disp('7.7')
disp('atand((%e^((sqrt(7)) / (2) - log((R) / (1e5)))) /(hex2dec(''aabb''))) =') //'log' function returns the natural logarithm of an argument
disp(atand((%e^((sqrt(7)) / (2) - log((R) / (1e5)))) /(hex2dec('aabb')))) //the argument here is in degrees

//7.8
disp('7.8')
disp('atoms = N * 1e-6 * (6 / 5)') //here the number of atoms in one mole of substance (N) is decreased respectively to the task
disp(N * 1e-6 * (6 / 5), 'atoms = ')

//7.9
disp('7.9')
disp('C_atoms = atoms * (2 / 9)') //this is the number of all carbon atoms from the previous task
disp('C13_atoms = C_atoms * (1 / 100)') //this is the number of C13 atoms from the previous task
disp('promiles =  C13_atoms * 1000 / atoms') //this is how many promiles of the substance is C13

disp(atoms * (2 / 9), 'C_atoms = ')
disp(C_atoms * (1 / 100), 'C13_atoms = ')
disp(C13_atoms * 1000 / atoms, 'promiles = ')
// I got an error when executing the script: Undefined variable: atoms

